package com.omicron.SpringGestioneCarte.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.omicron.SpringGestioneCarte.models.Carta;
import com.omicron.SpringGestioneCarte.models.Persona;
import com.omicron.SpringGestioneCarte.services.CartaService;

@RestController
@RequestMapping("/carta")
public class CartaController {

	@Autowired
	private CartaService service;
	
	@PostMapping("/inserisci")
	public Carta inserisciOggetto(@RequestBody Carta objPersona) {
		return service.insert(objPersona);
	}
	
	@GetMapping("/{id}")
	public Carta ricercaOggettoPerId(@PathVariable int id) {
		return service.findById(id);
	}
	
	@GetMapping("/")
	public List<Carta> ricercaTuttiOggetti(){
		return service.findAll();
	}
	
	@DeleteMapping("/{id}")
	public boolean eliminaOggetto(@PathVariable int id) {
		return service.delete(id);
	}
	
	@PutMapping("/modifica")
	public boolean modificaOggetto(@RequestBody Carta objPersona) {
		return service.update(objPersona);
	}
	
	@GetMapping("/associa/{id_carta}/{id_persona}")
	public boolean associaCartaEProprietario(@PathVariable int id_carta, @PathVariable int id_persona) {
		return service.associa(id_carta, id_persona);
	}
	
}
