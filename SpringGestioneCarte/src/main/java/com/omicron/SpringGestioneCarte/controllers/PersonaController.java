package com.omicron.SpringGestioneCarte.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.omicron.SpringGestioneCarte.models.Persona;
import com.omicron.SpringGestioneCarte.services.PersonaService;

@RestController
@RequestMapping("/persona")
public class PersonaController {

	@Autowired
	private PersonaService service;
	
	@PostMapping("/inserisci")
	public Persona inserisciOggetto(@RequestBody Persona objPersona) {
		return service.insert(objPersona);
	}
	
	@GetMapping("/{id}")
	public Persona ricercaOggettoPerId(@PathVariable int id) {
		return service.findById(id);
	}
	
	@GetMapping("/")
	public List<Persona> ricercaTuttiOggetti(){
		return service.findAll();
	}
	
	@DeleteMapping("/{id}")
	public boolean eliminaOggetto(@PathVariable int id) {
		return service.delete(id);
	}
	
	@PutMapping("/modifica")
	public boolean modificaOggetto(@RequestBody Persona objPersona) {
		return service.update(objPersona);
	}
	
}
