package com.omicron.SpringGestioneCarte.services;

import java.util.List;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;

import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.omicron.SpringGestioneCarte.models.Carta;
import com.omicron.SpringGestioneCarte.models.Persona;
import com.omicron.SpringGestioneCarte.repos.DataAccessRepo;

@Service
public class PersonaService implements DataAccessRepo<Persona>{

	@Autowired
	private EntityManager entityManager;
	
	private Session getSessione() {
		return entityManager.unwrap(Session.class);
	}
	
	@Override
	public Persona insert(Persona t) {

		Persona temp = new Persona();
		temp.setNome(t.getNome());
		temp.setCognome(t.getCognome());
		temp.setCodice_fiscale(t.getCodice_fiscale());
		
		Session sessione = getSessione();
		
		try {
			sessione.save(temp);
			return temp;
		} catch (Exception e) {
			System.out.println(e.getMessage());
			return null;
		} finally {
			sessione.close();
		}
		
	}

	@Override
	public Persona findById(int id) {
		Session sessione = getSessione();
		return (Persona) sessione.createCriteria(Persona.class)
				.add(Restrictions.eqOrIsNull("id", id)).uniqueResult();
	}

	@Override
	public List<Persona> findAll() {
		Session sessione = getSessione();
		return sessione.createCriteria(Persona.class).list();
	}

	@Transactional
	@Override
	public boolean delete(int id) {
		Session sessione = getSessione();
		
		try {
			
			Persona temp = sessione.load(Persona.class, id);
			sessione.delete(temp);
			sessione.flush();
			
			return true;
			
		} catch (Exception e) {
			System.out.println(e.getMessage());
			return false;
		} finally {
			sessione.close();
		}
	}

	@Transactional
	@Override
	public boolean update(Persona t) {
		Session sessione = getSessione();
		
		try {
			
			Persona temp = sessione.load(Persona.class, t.getId());
			
			if(t.getNome() != null)
				temp.setNome(t.getNome());
			if(t.getCognome() != null)
				temp.setCognome(t.getCognome());
			if(t.getCodice_fiscale() != null)
				temp.setCodice_fiscale(t.getCodice_fiscale());
			
			sessione.save(temp);
			sessione.flush();
			
			return true;
			
		} catch (Exception e) {
			System.out.println(e.getMessage());
			return false;
		} finally {
			sessione.close();
		}
	}

}
