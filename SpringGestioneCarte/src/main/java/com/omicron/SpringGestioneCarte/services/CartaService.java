package com.omicron.SpringGestioneCarte.services;

import java.util.List;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;

import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.omicron.SpringGestioneCarte.models.Carta;
import com.omicron.SpringGestioneCarte.models.Persona;
import com.omicron.SpringGestioneCarte.repos.DataAccessRepo;

@Service
public class CartaService implements DataAccessRepo<Carta>{

	@Autowired
	private EntityManager entityManager;
	
	private Session getSessione() {
		return entityManager.unwrap(Session.class);
	}
	
	@Transactional
	public boolean associa(int carId, int perId) {
		
		Session sessione = getSessione();
		
		try {
			
			Persona tempPer = sessione.load(Persona.class, perId);			//Sostituisci questo metodo con una richiesta al MS Persona
			Carta tempCar = sessione.load(Carta.class, carId);
			
			tempCar.setProprietario(tempPer);
			sessione.save(tempCar);
			
			sessione.flush();
			return true;
			
		} catch (Exception e) {
			System.out.println(e.getMessage());
			return false;
		} finally {
			sessione.close();
		}
		
	}
	
	@Override
	public Carta insert(Carta t) {

		Carta temp = new Carta();
		temp.setNegozio(t.getNegozio());
		temp.setNumero(t.getNumero());
		
		Session sessione = getSessione();
		
		try {
			sessione.save(temp);
			return temp;
		} catch (Exception e) {
			System.out.println(e.getMessage());
			return null;
		} finally {
			sessione.close();
		}
		
	}

	@Override
	public Carta findById(int id) {
		Session sessione = getSessione();
		return (Carta) sessione.createCriteria(Carta.class)
				.add(Restrictions.eqOrIsNull("id", id)).uniqueResult();
	}

	@Override
	public List<Carta> findAll() {
		Session sessione = getSessione();
		return sessione.createCriteria(Carta.class).list();
	}

	@Transactional
	@Override
	public boolean delete(int id) {
		Session sessione = getSessione();
		
		try {
			
			Carta temp = sessione.load(Carta.class, id);
			sessione.delete(temp);
			sessione.flush();
			
			return true;
			
		} catch (Exception e) {
			System.out.println(e.getMessage());
			return false;
		} finally {
			sessione.close();
		}
	}

	@Transactional
	@Override
	public boolean update(Carta t) {
		Session sessione = getSessione();
		
		try {
			
			Carta temp = sessione.load(Carta.class, t.getId());
			
			if(t.getNegozio() != null)
				temp.setNegozio(t.getNegozio());
			if(t.getNumero() != null)
				temp.setNumero(t.getNumero());
			
			sessione.save(temp);
			sessione.flush();
			
			return true;
			
		} catch (Exception e) {
			System.out.println(e.getMessage());
			return false;
		} finally {
			sessione.close();
		}
	}

}
