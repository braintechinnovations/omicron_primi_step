package com.omicron.SpringGestioneCarte.models;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="persona")
public class Persona {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="personaID")
	private int id;
	
	@Column
	private String nome;
	@Column
	private String cognome;
	@Column(name="cod_fis")
	private String codice_fiscale;
	
	@OneToMany(mappedBy = "proprietario")
	private List<Carta> elencoCarte;

	public Persona() {
		
	}
	
	public Persona(String nome, String cognome, String codice_fiscale) {
		super();
		this.nome = nome;
		this.cognome = cognome;
		this.codice_fiscale = codice_fiscale;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getCognome() {
		return cognome;
	}
	public void setCognome(String cognome) {
		this.cognome = cognome;
	}
	public String getCodice_fiscale() {
		return codice_fiscale;
	}
	public void setCodice_fiscale(String codice_fiscale) {
		this.codice_fiscale = codice_fiscale;
	}

	public List<Carta> getElencoCarte() {
		return elencoCarte;
	}

	public void setElencoCarte(List<Carta> elencoCarte) {
		this.elencoCarte = elencoCarte;
	}

	@Override
	public String toString() {
		return "Persona [id=" + id + ", nome=" + nome + ", cognome=" + cognome + ", codice_fiscale=" + codice_fiscale
				+ ", elencoCarte=" + elencoCarte + "]";
	}
	
	
	public String stampaProprietarioSafe() {
		return "Persona [id=" + id + ", nome=" + nome + ", cognome=" + cognome + ", codice_fiscale=" + codice_fiscale
				+ "]";
	}
	
	
	
}
