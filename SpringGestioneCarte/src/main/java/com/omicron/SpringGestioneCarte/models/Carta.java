package com.omicron.SpringGestioneCarte.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="carta")
public class Carta {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="CartaID")
	private int id;
	
	@Column
	private String numero;
	@Column
	private String negozio;
	
	@ManyToOne
	@JoinColumn(name="persona_rif")
	private Persona proprietario;
	
	public Carta() {
		
	}

	public Carta(String numero, String negozio) {
		super();
		this.numero = numero;
		this.negozio = negozio;
	}
	
	public Carta(String numero, String negozio, Persona proprietario) {
		super();
		this.numero = numero;
		this.negozio = negozio;
		this.proprietario = proprietario;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public String getNegozio() {
		return negozio;
	}

	public void setNegozio(String negozio) {
		this.negozio = negozio;
	}

	public Persona getProprietario() {
		return proprietario;
	}

	public void setProprietario(Persona proprietario) {
		this.proprietario = proprietario;
	}

	@Override
	public String toString() {
		return "Carta [id=" + id + ", numero=" + numero + ", negozio=" + negozio + ", proprietario=" + proprietario
				+ "]";
	}
	
	public String stampaCartaSafe() {
		return "Carta [id=" + id + ", numero=" + numero + ", negozio=" + negozio + ", proprietario=" + proprietario.stampaProprietarioSafe()
				+ "]";
	}
	
	
	
}
