package corso.omicron.HibernateManyToMany.models;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

@Entity
@Table(name="esame")
public class Esame {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="esameID")
	private int id;
	
	@Column
	private String titolo;
	@Column
	private Integer crediti;
	@Column
	private String data_esame;
	
	@ManyToMany
	@JoinTable(
			name="studente_esame",
			joinColumns = { @JoinColumn(name="esame_rif", referencedColumnName = "esameID") },
			inverseJoinColumns = { @JoinColumn(name="studente_rif", referencedColumnName = "studenteID") }
			)
	private List<Studente> elencoStudenti;
	
	public Esame() {
		
	}
	
	public Esame(String titolo, Integer crediti, String data_esame) {
		super();
		this.titolo = titolo;
		this.crediti = crediti;
		this.data_esame = data_esame;
	}
	
	public List<Studente> getElencoStudenti() {
		return elencoStudenti;
	}

	public void setElencoStudenti(List<Studente> elencoStudenti) {
		this.elencoStudenti = elencoStudenti;
	}

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getTitolo() {
		return titolo;
	}
	public void setTitolo(String titolo) {
		this.titolo = titolo;
	}
	public Integer getCrediti() {
		return crediti;
	}
	public void setCrediti(Integer crediti) {
		this.crediti = crediti;
	}
	public String getData_esame() {
		return data_esame;
	}
	public void setData_esame(String data_esame) {
		this.data_esame = data_esame;
	}
	
	
	
}
