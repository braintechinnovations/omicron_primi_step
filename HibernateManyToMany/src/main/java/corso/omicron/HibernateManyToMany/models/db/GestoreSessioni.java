package corso.omicron.HibernateManyToMany.models.db;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import corso.omicron.HibernateManyToMany.models.Esame;
import corso.omicron.HibernateManyToMany.models.Studente;

public class GestoreSessioni {

	private static GestoreSessioni ogg_gestore;
	private SessionFactory factory;
	
	public static GestoreSessioni getIstanza() {
		if(ogg_gestore == null) {
			ogg_gestore = new GestoreSessioni();
		}
		
		return ogg_gestore;
	}
	
	public SessionFactory getFactory() {
		if(factory == null) {
			
			factory = new Configuration()
					.configure("/resources/hibernate.cfg.xml")
					.addAnnotatedClass(Esame.class)
					.addAnnotatedClass(Studente.class)
					.buildSessionFactory();
			
		}
		
		return factory;
	}
	
}
