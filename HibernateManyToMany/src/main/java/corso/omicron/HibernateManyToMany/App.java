package corso.omicron.HibernateManyToMany;

import java.util.ArrayList;

import org.hibernate.Session;

import corso.omicron.HibernateManyToMany.models.Esame;
import corso.omicron.HibernateManyToMany.models.Studente;
import corso.omicron.HibernateManyToMany.models.db.GestoreSessioni;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {

    	Session sessione = GestoreSessioni.getIstanza().getFactory().getCurrentSession();

    	try {
			
    		Studente mario = new Studente("Mario", "Rossi", "AB123456");
    		
    		Esame fis = new Esame("Fisica I", 6, "2021-10-01");
    		Esame ana = new Esame("Analisi I", 6, "2021-10-02");
    		
    		mario.setElencoEsami(new ArrayList<Esame>());
    		mario.getElencoEsami().add(fis);
    		mario.getElencoEsami().add(ana);
    		
    		sessione.beginTransaction();
    		
    		sessione.save(fis);
    		sessione.save(ana);
    		sessione.save(mario);
    		
    		sessione.getTransaction().commit();
    		
    		
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
    	
		sessione.close();
    	
    }
}
