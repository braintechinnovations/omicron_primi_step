package com.omicron.SpringGestioneUniversita.models;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

@Entity
@Table(name="studente")
public class Studente {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="studenteID")
	private int id;
	
	@Column
	private String nome;
	@Column
	private String cognome;
	@Column
	private String matricola;
	
	@ManyToMany
	@JoinTable(
			name="studente_esame",
			joinColumns = { @JoinColumn(name="studente_rif", referencedColumnName = "studenteID") },
			inverseJoinColumns = { @JoinColumn(name="esame_rif", referencedColumnName = "esameID") }
			)
	private List<Esame> elencoEsami;
	
	public Studente() {
		
	}
	
	public Studente(String nome, String cognome, String matricola) {
		super();
		this.nome = nome;
		this.cognome = cognome;
		this.matricola = matricola;
	}
	
	
	public List<Esame> getElencoEsami() {
		return elencoEsami;
	}

	public void setElencoEsami(List<Esame> elencoEsami) {
		this.elencoEsami = elencoEsami;
	}

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getCognome() {
		return cognome;
	}
	public void setCognome(String cognome) {
		this.cognome = cognome;
	}
	public String getMatricola() {
		return matricola;
	}
	public void setMatricola(String matricola) {
		this.matricola = matricola;
	}

	@Override
	public String toString() {
		return "Studente [id=" + id + ", nome=" + nome + ", cognome=" + cognome + ", matricola=" + matricola + "]";
	}
	
	
}
