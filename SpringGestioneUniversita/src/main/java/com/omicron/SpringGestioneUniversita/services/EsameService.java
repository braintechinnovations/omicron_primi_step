package com.omicron.SpringGestioneUniversita.services;

import java.util.List;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;

import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.omicron.SpringGestioneUniversita.models.Esame;
import com.omicron.SpringGestioneUniversita.repos.DataAccessRepo;

@Service
public class EsameService implements DataAccessRepo<Esame> {

	@Autowired
	private EntityManager entityManager;
	
	private Session getSessione() {
		return entityManager.unwrap(Session.class);
	}
	
	@Override
	public Esame insert(Esame t) {

		Esame temp = new Esame();
		temp.setTitolo(t.getTitolo());
		temp.setCrediti(t.getCrediti());
		temp.setData_esame(t.getData_esame());
		
		Session sessione = getSessione();
		
		try {
			sessione.save(temp);
			return temp;
		} catch (Exception e) {
			System.out.println(e.getMessage());
			return null;
		} finally {
			sessione.close();
		}
		
	}

	@Override
	public Esame findById(int id) {

		Session sessione = getSessione();
		
		return (Esame) sessione.createCriteria(Esame.class)
				.add(Restrictions.eqOrIsNull("id", id)).uniqueResult();
		
	}

	@Override
	public List<Esame> findAll() {
		Session sessione = getSessione();
		
		return sessione.createCriteria(Esame.class).list();
	}

	@Transactional
	@Override
	public boolean delete(int id) {
		
		Session sessione = getSessione();
		
		try {
			
			Esame temp = sessione.load(Esame.class, id);
			sessione.delete(temp);
			sessione.flush();
			
			return true;
			
		} catch (Exception e) {
			System.out.println(e.getMessage());
			return false;
		} finally {
			sessione.close();
		}
	}

	@Transactional
	@Override
	public boolean update(Esame t) {
		
		Session sessione = getSessione();
		
		try {
			
			Esame temp = sessione.load(Esame.class, t.getId());
			
			if(t.getTitolo() != null)
				temp.setTitolo(t.getTitolo());
			if(t.getCrediti() != null)
				temp.setCrediti(t.getCrediti());
			if(t.getData_esame() != null)
				temp.setData_esame(t.getData_esame());
			
			sessione.save(temp);
			sessione.flush();
			
			return true;
			
		} catch (Exception e) {
			System.out.println(e.getMessage());
			return false;
		} finally {
			sessione.close();
		}
		
	}
	
}
