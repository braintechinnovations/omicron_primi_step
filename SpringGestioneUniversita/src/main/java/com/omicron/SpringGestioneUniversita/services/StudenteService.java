package com.omicron.SpringGestioneUniversita.services;

import java.util.List;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;

import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.omicron.SpringGestioneUniversita.models.Studente;
import com.omicron.SpringGestioneUniversita.repos.DataAccessRepo;

@Service
public class StudenteService implements DataAccessRepo<Studente>{

	@Autowired
	private EntityManager entityManager;
	
	private Session getSessione() {
		return entityManager.unwrap(Session.class);
	}
	
	@Override
	public Studente insert(Studente t) {

		Studente temp = new Studente();
		temp.setNome(t.getNome());
		temp.setCognome(t.getCognome());
		temp.setMatricola(t.getMatricola());
		
		Session sessione = getSessione();
		
		try {
			sessione.save(temp);
			return temp;
		} catch (Exception e) {
			System.out.println(e.getMessage());
			return null;
		} finally {
			sessione.close();
		}
		
	}

	@Override
	public Studente findById(int id) {

		Session sessione = getSessione();
		
		return (Studente) sessione.createCriteria(Studente.class)
				.add(Restrictions.eqOrIsNull("id", id)).uniqueResult();
		
	}

	@Override
	public List<Studente> findAll() {
		Session sessione = getSessione();
		
		return sessione.createCriteria(Studente.class).list();
	}

	@Transactional
	@Override
	public boolean delete(int id) {
		
		Session sessione = getSessione();
		
		try {
			
			Studente temp = sessione.load(Studente.class, id);
			sessione.delete(temp);
			sessione.flush();
			
			return true;
			
		} catch (Exception e) {
			System.out.println(e.getMessage());
			return false;
		} finally {
			sessione.close();
		}
	}

	@Transactional
	@Override
	public boolean update(Studente t) {
		
		Session sessione = getSessione();
		
		try {
			
			Studente temp = sessione.load(Studente.class, t.getId());
			
			if(t.getNome() != null)
				temp.setNome(t.getNome());
			if(t.getCognome() != null)
				temp.setCognome(t.getCognome());
			if(t.getMatricola() != null)
				temp.setMatricola(t.getMatricola());
			
			sessione.save(temp);
			sessione.flush();
			
			return true;
			
		} catch (Exception e) {
			System.out.println(e.getMessage());
			return false;
		} finally {
			sessione.close();
		}
		
	}

	
	
}
