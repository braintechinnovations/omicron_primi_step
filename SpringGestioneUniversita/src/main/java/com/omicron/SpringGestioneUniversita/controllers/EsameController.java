package com.omicron.SpringGestioneUniversita.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.omicron.SpringGestioneUniversita.models.Esame;
import com.omicron.SpringGestioneUniversita.services.EsameService;

@RestController
@RequestMapping("/esame")
public class EsameController {
	
	@Autowired
	private EsameService service;
	
	@PostMapping("/inserisci")
	public Esame inserisciOggetto(@RequestBody  Esame obj) {
		return service.insert(obj);
	}
	
	@GetMapping("/{id}")
	public  Esame ricercaOggettoPerId(@PathVariable int id) {
		return service.findById(id);
	}
	
	@GetMapping("/")
	public List<Esame> ricercaTuttiOggetti(){
		return service.findAll();
	}
	
	@DeleteMapping("/{id}")
	public boolean eliminaOggetto(@PathVariable int id) {
		return service.delete(id);
	}
	
	@PutMapping("/modifica")
	public boolean modificaOggetto(@RequestBody Esame obj) {
		return service.update(obj);
	}
}
