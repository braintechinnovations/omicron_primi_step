package com.omicron.SpringGestioneUniversita.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.omicron.SpringGestioneUniversita.models.Esame;
import com.omicron.SpringGestioneUniversita.models.Studente;
import com.omicron.SpringGestioneUniversita.services.EsameService;
import com.omicron.SpringGestioneUniversita.services.StudenteService;

@RestController
@RequestMapping("/iscrizioni")
public class IscrizioniController {

	@Autowired
	private EsameService esameService;
	@Autowired
	private StudenteService studenteService;
	
	@GetMapping("/iscrivi/{id_studente}/{id_esame}")
	public boolean iscriviStudenteEsame(@PathVariable int id_studente, @PathVariable int id_esame) {
		
		Studente studTemp = studenteService.findById(id_studente);	//Stostituta con una chiamata al WS Studente
		Esame esamTemp = esameService.findById(id_esame);			//Stostituta con una chiamata al WS Esame
		
		if(studTemp != null && esamTemp != null) {
			studTemp.getElencoEsami().add(esamTemp);				//Sostituito con il salvataggio nel mio DB della relazione
			return true;
		}
		else {
			return false;
		}
		
		
	}
	
}
