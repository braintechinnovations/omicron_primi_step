package com.omicron.SpringGestioneUniversita.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.omicron.SpringGestioneUniversita.models.Studente;
import com.omicron.SpringGestioneUniversita.services.StudenteService;

@RestController
@RequestMapping("/studente")

public class StudenteController {
	@Autowired
	private StudenteService service;
	
	@PostMapping("/inserisci")
	public Studente inserisciOggetto(@RequestBody  Studente obj) {
		return service.insert(obj);
	}
	
	@GetMapping("/{id}")
	public  Studente ricercaOggettoPerId(@PathVariable int id) {
		return service.findById(id);
	}
	
	@GetMapping("/")
	public List<Studente> ricercaTuttiOggetti(){
		return service.findAll();
	}
	
	@DeleteMapping("/{id}")
	public boolean eliminaOggetto(@PathVariable int id) {
		return service.delete(id);
	}
	
	@PutMapping("/modifica")
	public boolean modificaOggetto(@RequestBody Studente obj) {
		return service.update(obj);
	}
}
