package com.omicron.SpringGestioneUniversita;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringGestioneUniversitaApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringGestioneUniversitaApplication.class, args);
	}

}
