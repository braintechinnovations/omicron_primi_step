package com.omicron.SpringGestioneUniversita.repos;

import java.util.List;

public interface DataAccessRepo<T> {
	
	T insert(T t);
	T findById(int id);
	List<T> findAll();
	boolean delete(int id);
	boolean update(T t);

}
