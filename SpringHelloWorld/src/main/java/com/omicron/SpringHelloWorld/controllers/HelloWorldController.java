package com.omicron.SpringHelloWorld.controllers;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.omicron.SpringHelloWorld.models.Persona;

@RestController
@RequestMapping("/helloworld")
public class HelloWorldController {

//	@GetMapping("/hello")
//	public String dimmiCiao() {
//		return "Ciao Giovanni!";
//	}
//	
//	@GetMapping("/hello/mario")
//	public String salutaMario() {
//		System.out.println("Ciao, sono la SYSO");
//		return "Ciao Mario";
//	}
	
	//http://localhost:8080/hello/mario
	//<protocollo>://<indirizzo_server>:<porta>/<segment>/<segment>
	//<protocollo>://<root>/<segment>/<segment>
	//<protocollo>://<base_path>/<segment>/<segment>
	
	@GetMapping("/hello/{nome}")
	public String salutaGenerico(@PathVariable String nome) {
		return "Ciao " + nome;
	}
	
	//http://localhost:8080/variabili?nome=Giovanni&cognome=Pace
	@GetMapping("/variabili")
	public String restituisciVariabili(@RequestParam String nome, @RequestParam String cognome) {
		return "Ciao " + nome + " " + cognome;
	}
	

	@GetMapping("/variabiliconnome")
	public String restituisciVariabiliConNome(
			@RequestParam(name="varNome") String nome, 
			@RequestParam(name="varCognome") String cognome) {
		return "Ciao " + nome + " " + cognome;
	}
	
	@PostMapping("/hellopost")
	public String salutaPost() {
		return "Ciao, sono la richiesta Post";
	}
	
	@PostMapping("/persona/giovanni")
	public Persona restituisciGiovanni() {
		Persona gio = new Persona("Giovanni", "Pace");
		return gio;
	}
	
	@PostMapping("/persona/saluta")
	public Persona salutaPersona(@RequestBody Persona temp){
		
		return temp;
		
	}
	
	
}
