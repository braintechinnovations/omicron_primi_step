package com.omicron.SpringHelloWorld.controllers;

import java.util.ArrayList;
import java.util.List;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.omicron.SpringHelloWorld.models.Persona;

@RestController
@RequestMapping("/persona")
public class PersonaController {

	@PostMapping("/saluta")
	public Persona salutaPersona(@RequestBody Persona temp){
		return temp;
	}
	
	@GetMapping("/lista")
	public List<Persona> listaPersone(){
		Persona gio = new Persona("Giovanni", "Pace");
		Persona mar = new Persona("Mario", "Rossi");
		Persona val = new Persona("Valeria", "Verdi");
		
		List<Persona> elenco = new ArrayList<Persona>();
		elenco.add(val);
		elenco.add(mar);
		elenco.add(gio);
		
		return elenco;
	}
	
	@PostMapping("/importa")
	public List<Persona> importaPersone(@RequestBody List<Persona> elenco){
		return elenco;
	}
	
}
